#!/bin/sh

set -e

run_server() {
  env=$1
  version=$2

  if [ "${env}" = "dev" ]; then
    echo "stop mercurius"
    docker stop mercurius || true
    docker rm mercurius || true

    echo "run mercurius"
    docker run -d --name mercurius -p 8088:8080 -e APP_ENV=${env} mercurius:${version}
  else
    echo "env not dev!"
  fi
}

case $1 in
run)
  run_server $2 $3
  ;;
esac