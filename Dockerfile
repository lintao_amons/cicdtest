FROM openjdk:11

ENV APP_ENV dev

COPY ./build/libs/*.jar /app/mercurius.jar
COPY ./run.sh /app/

EXPOSE 8088
CMD ["/app/run.sh"]